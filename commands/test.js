module.exports = {
    name: 'test',
    description: 'Pong !',
    async execute(message, args) {
        const { PREFIX } = require('../config.js');
        const name = args[0];
        const category = await message.guild.channels.cache.find(cat => cat.name === name); // You can use `find` instead of `get` to fetch the category using a name: `find(cat => cat.name === 'test')
        const role = await message.guild.roles.cache.find(role => role.name === name);

        category.children.forEach(channel => channel.delete())
        category.delete()
        role.delete()

        message.channel.send(`${name} a bien été désinscrit !`)

        // if (!name) {
        //     return message.channel.send(`${name} n'éxiste pas !`)
        // }




    }
}