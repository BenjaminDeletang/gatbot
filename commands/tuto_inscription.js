const  { MessageEmbed } = require ("discord.js");

module.exports = {
    name: 'tuto-inscription',
    description: 'creation de role',
    async execute(message, args, client) {
        const { PREFIX } = require('../config.js');

        const embedIns = new MessageEmbed()
            .setTitle(`**Aide pour les insciptions aux tournois**`)
            .setColor('#e79b0c')
            .setFooter('Edit by Lc-Killer49')
            .setDescription('`?insc NomEquipe @MentionCoéquipiers`' +
                '\n\nExemple pour un tournoi TRIO :' +
                '\n`?insc Gatlinger @Valkyrie @CouKou_Twaa`' +
                '\n\nLe capitaine (Celui qui fait la commande) n\'a pas besoin de mentionner son propre nom.' +
                '\n\nExemple pour un tournoi SOLO :' +
                '\n`?insc Gatlinger`'+
                '\n\n**/!\\ Attention**'+
                '\nLe nom de team ne doit pas contenir d\'espace (Utilises des _ ou des -).' +
                '\nExemple : La_Team_Rocket'+
                '\n\nUne fois ton inscription terminée, 2 channels seront créés uniquement pour votre équipe. Un channel écrit et un vocal')
        const embedDes = new MessageEmbed()
            .setTitle(`**Aide pour la désinscription d'un tournois**`)
            .setColor('#9b0014')
            .setFooter('Edit by Lc-Killer49')
            .setDescription('`?des NomEquipe`' +
                '\n\n**/!\\ Attention**'+
                '\nSeul le capitaine de l\' équipe(Celui qui à fait la commande pour s\'inscrire au tournois) peut quitter le tournois.')
        message.channel.send(embedIns)
        message.channel.send(embedDes)


        if (message.content.toLowerCase() === `${PREFIX}`+this.name) {
            await message.delete()}
    }
}