const  { MessageEmbed, MessageAttachment } = require ("discord.js");

module.exports = {
    name: 'help',
    description: 'creation de role',
    async execute(message, args, client) {
        const { PREFIX } = require('../config.js');
        const nameguild = message.guild.name;

        //Serveur Gatlinger Tournament
        if (['809420221961207828'].includes(message.guild.id)) {
            const embedGatT = new MessageEmbed()
                .setTitle(`Liste des commandes du serveur ${nameguild}`)
                .setFooter('Edit by Lc-Killer49')
                .setDescription(`${PREFIX}help` +
                    '\nAffiche le menu help' +
                    `\n\n${PREFIX}insc NomEquipe <@pseudo2> <@pseudo3>`+
                    '\nInscription de son équipe' +
                    `\n\n${PREFIX}des NomEquipe` +
                    '\nDésinscription complète de son équipe' )
            const embedGatTAdmin = new MessageEmbed()
                .setTitle('Liste des commandes du Staff')
                .setFooter('Edit by Lc-Killer49')
                .setDescription(`${PREFIX}sond`+
                    '\nCréation d\'un sondage avec "✅" et "❌"' +
                    `\n\n${PREFIX}tuto-inscription`+
                    '\nTuto qui explique comment s\'inscrire et ce désinscrire aux tournois')
            message.channel.send(embedGatT)
            if (message.member.hasPermission("ADMINISTRATOR")) {
                message.channel.send(embedGatTAdmin)
            }
        } else //tout sauf le serveur de tournois
            {
            const embedGat = new MessageEmbed()
                .setTitle(`Liste des commandes possible sur le serveur ${nameguild}`)
                .setFooter('Edit by Lc-Killer49')
                .setDescription(`${PREFIX}help` +
                    '\nAffiche le menu help')
            const embedGatAdmin = new MessageEmbed()
                .setTitle('Liste des commandes du Staff')
                .setFooter('Edit by Lc-Killer49')
                .setDescription(`${PREFIX}sond`+
                    '\nCréation d\'un sondage avec "✅" et "❌"')
            message.channel.send(embedGat)
            if (message.member.hasPermission("ADMINISTRATOR")) {
                message.channel.send(embedGatAdmin)
            }
        }
        if (message.content.toLowerCase() === `${PREFIX}`+this.name) {
            await message.delete()}
    }
}