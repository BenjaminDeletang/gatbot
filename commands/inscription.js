module.exports = {
    name: 'insc',
    description: 'Inscription des equipe',
    async execute(message, args, user) {
        // you already receive the args, the first one is the team name
        const name = args[0];

        const rolecheck = message.guild.roles.cache.find((role) => {
            return role.name === name
        })
        if (!rolecheck) {
            // create role
            const role = await message.guild.roles.create({ data: { name, color: '#e79b0c' } });
            const permissionOverwrites = [
                // deny access to everyone
                { id: message.guild.id, deny: ['VIEW_CHANNEL'] },
                // use the role id to allow users with the role to access the channel
                { id: role.id, allow: ['VIEW_CHANNEL'] },
            ];

            // add role to mentioned members if there are any
            message.mentions.members.each((member) => {
                message.member.roles.add(role)
                member.roles.add(role.id);
            });

            // create a category and grab the created channel
            const category = await message.guild.channels.create(name, {
                type: 'category',
                permissionOverwrites,
            });

            message.guild.channels.create(name, {
                type: 'text',
                parent: category,
                permissionOverwrites,
            });

            message.guild.channels.create(name, {
                type: 'voice',
                parent: category,
                permissionOverwrites,
            });

            return message.channel.send(`L'inscription à bien été effetué pour l'équipe ${name}`);
        }
        else return message.channel.send(`${name} existe déjà, choisissez un autre nom de team`)

    },
};