module.exports = {
    name: 'sond',
    description: 'Inscription des equipe',
    permissions: 'ADMINISTRATOR',
    permissionsError: 'Vous devez avoir les droit Administrateur pour utiliser cette commande',
    async execute(message, args, client) {
        const { PREFIX } = require('../config.js');
        const channelIds = [
            // '809768947447889942',
        ]

        const addReactions = (message) => {
            message.react('✅')

            setTimeout(() => {
                message.react('❌')
            }, 750)
        }

        if (channelIds.includes(message.channel.id)) {
            addReactions(message)
        } else if (message.content.toLowerCase() === `${PREFIX}`+ this.name) {
            await message.delete()

            const fetched = await message.channel.messages.fetch({limit: 1})
            if (fetched && fetched.first()) {
                addReactions(fetched.first())
            }
        }
    }
}