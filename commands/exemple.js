module.exports = {
    name: 'exemple',
    description: 'Pong !',
    permissions: 'ADMINISTRATOR',
    permissionsError: 'Vous devez avoir les droit Administrateur pour utiliser cette commande',
    execute(message, args) {
        message.channel.send('Pong !')
    }
}